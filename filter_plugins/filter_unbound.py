def ubool(x):
    return ['no', 'yes'][bool(x)]

class FilterModule(object):
    ''' unbound role jinja2 filters '''

    def filters(self):
        return {
            'ubool': ubool
        }
